package gattanjonilsson.pling;

/**
 * Created by Jo on 2017-01-10.
 */

public class Task {

    String name;
    Boolean completed = false;

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean getCompleted() {
        return true;
    }

}
